import React from 'react';

class PresentationForm extends React.Component {

constructor(props) {
  super(props)

  this.state = {
    name: '',
    email: '',
    companyName: '',
    title: '',
    synopsis: '',
    conferences: [],

  };

  this.handleNameChange = this.handleNameChange.bind(this);
  this.handleEmailChange = this.handleEmailChange.bind(this);
  this.handleCompanyNameChange = this.handleCompanyNameChange.bind(this);
  this.handleTitleChange = this.handleTitleChange.bind(this);
  this.handleSynopsisChange = this.handleSynopsisChange.bind(this);
  this.handleConferenceChange = this.handleConferenceChange.bind(this);
  this.handleSubmit = this.handleSubmit.bind(this);

}


handleNameChange(event) {
   const value = event.target.value;
   this.setState({name: value})
}

handleEmailChange(event) {
  const value = event.target.value;
  this.setState({email: value})
}

handleCompanyNameChange (event) {
  const value = event.target.value;
  this.setState({companyName: value})
}

handleTitleChange (event) {
  const value = event.target.value;
  this.setState({title: value})
}

handleSynopsisChange (event) {
  const value = event.target.value;
  this.setState({synopsis: value})
}

handleConferenceChange(event) {
  const value = event.target.value;
  this.setState({conference: value})
}

// to register and post fetch when hitting submit button
async handleSubmit (event) {
  event.preventDefault();
  const data = {...this.state};
  data.presenter_name = data.name;
  data.presenter_email = data.email;
  data.company_name = data.companyName;
  delete data.conferences;
  delete data.name;
  delete data.email;
  delete data.companyName;
  const href = data.conference;
  console.log("submit data: ", data, "HREF: ", href);

//  add post fetch and clear data here:

const url = `http://localhost:8000${href}presentations/`;
const fetchConfig = {
  method: "post",
  body: JSON.stringify(data),
  headers: {
    'Content-Type': 'application/json',
  },
};

const response = await fetch(url, fetchConfig);
if (response.ok) {
  const newPresentation = await response.json();
  console.log("New Presentation: ", newPresentation);
}


//  to clear form:
const cleared = {
  name: '',
  email: '',
  companyName: '',
  title: '',
  synopsis: '',
  conference: '',
};

this.setState(cleared);

}

 // to populate conference dropdown
  async componentDidMount() {
    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      this.setState({conferences: data.conferences});

    }
  }

render() {


  return(
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Create a new presentation</h1>
        <form onSubmit={this.handleSubmit} id="create-location-form">
          <div className="form-floating mb-3">
            <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" id="presenter_name" name="presenter_name" className="form-control"/>
            <label htmlFor="name">Presenter name</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={this.handleEmailChange} value={this.state.email} placeholder="Email" required type="text" id="presenter_email" name="presenter_email" className="form-control"/>
            <label htmlFor="name">Presenter email</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={this.handleCompanyNameChange} value={this.state.companyName} placeholder="Company name" required type="text" id="company_name" name="company_name" className="form-control"/>
            <label htmlFor="name">Company name</label>
           </div>
            <div className="form-floating mb-3">
              <input onChange={this.handleTitleChange} value={this.state.title} placeholder="Title" required type="text" id="title" name="title" className="form-control"/>
              <label htmlFor="name">Title</label>
            </div>
          <div className="mb-3">
            <label htmlFor="synopsis" className="form-label">Synopsis</label>
            <textarea onChange={this.handleSynopsisChange} value={this.state.synopsis} className="form-control" id="synopsis" name="synopsis" rows="3"></textarea>
          </div>
          <div className="mb-3">
            <select onChange={this.handleConferenceChange} required id="conference" name="conference" className="form-select">
              <option value="">Choose a conference</option>
              {this.state.conferences.map(conference => {
                      return (
                        <option key={conference.href} value={conference.href}>
                          {conference.name}
                        </option>
                      );
                    })}
            </select>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
        <div className="result"></div>
      </div>
    </div>
  </div>

  )
}

}

export default PresentationForm;
