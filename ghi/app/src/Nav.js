import React from 'react';
import { Link } from 'react-router-dom';
import { NavLink } from 'react-router-dom';

function Nav() {
  return (

<nav className="navbar navbar-expand-lg navbar-light bg-light">
<div className="container-fluid">
  <Link to="/" className="navbar-brand">Conference GO!</Link>
  <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>
  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
      <li className="nav-item">
        <Link to="/" className="nav-link" aria-current="page" >Home</Link>
      </li>
      <li className="nav-item">
        <Link to="/locations/new" id="nav-location" className="nav-link" aria-current="page">New location</Link>
      </li>
      <li className="nav-item">
        <Link to="/conferences/new" id="nav-conf" className="nav-link" aria-current="page">New conference</Link>
      </li>
      <li className="nav-item">
        <Link to="/presentations/new" className="nav-link" aria-current="page">New presentation</Link>
      </li>
      <li className="nav-item">
        <Link to="/attendees" className="nav-link" aria-current="page">List of Attendees</Link>
      </li>
      </ul>
        <Link to="/attendees/new" className="btn btn-primary">Attend!</Link>
  </div>
</div>
</nav>
  );
}

export default Nav;
