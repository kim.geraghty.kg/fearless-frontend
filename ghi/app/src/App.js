import React from 'react';
import Nav from './Nav';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeesForm from './AttendeesForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';



function App(props) {

  if (props.attendees === undefined) {
    return null;
  }

  return (

    <BrowserRouter>

    <div className="container">
    <React.Fragment>
    <Nav />
    </React.Fragment>
    </div>
<Routes>
<Route index element={<MainPage />} />
<Route path="/conferences/new" element={<ConferenceForm />}/>
</Routes>
<Routes>
<Route path="/attendees/new" element={<AttendeesForm />}/>
</Routes>
<Routes>
  // example of nested route:
  <Route path="locations">
<Route path="new" element={<LocationForm /> }/>
</Route>
</Routes>
<Routes>
<Route path="/attendees" element={<AttendeesList attendees={props.attendees}/>}/>
</Routes>
<Routes>
<Route path="/presentations/new" element={<PresentationForm/>}/>
</Routes>



    </BrowserRouter>

  );
}

export default App;
